const donarModel = require('../models/donar.model');
const donarService = require('../services/donar.service');

// Adding new donar in database
const createDonar = async (req, res) => {

    let donar = await donarService.findDonarByEmail(req.body);
    if (donar) {
        return res.status(400).send("Email already exist");
    }

    req.body.type = "Donar";
    req.body.createdAt = new Date();
    donar = await donarService.createDonar(req.body)

    return res.status(200).send(donar);
}


// Update a donar
const updateDonar = async (req, res) => {
    const donar = await donarService.updateDonarById(req.params.donarId, req.body);

    if (!donar) {
        res.status(400).send("Donar not found");
    }

    res.send(donar).status(200);
}


// Getting a donar by id
const getDonarById = async (req, res) => {

    const donar = await donarService.getDonarById(req.params.donarId);

    if (!donar) {
        res.status(400).send("Donar not found");
    }

    res.send(donar).status(200)
}

// Getting all donars
const getAllDonars = async (req, res) => {

    return res.status(200).send(await donarService.getAllDonars(req.query))
}


// Delete a donar by id
const deleteDonarById = () => {

}

const deleteAllDonars = async (req, res) => {

    await donarModel.deleteMany();
    return res.send(null).status(200)
}


module.exports = {
    createDonar,
    getDonarById,
    getAllDonars,
    updateDonar,
    deleteDonarById,
    deleteAllDonars
}