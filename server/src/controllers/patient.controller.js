const patientModel = require('../models/patient.model');
const patientService = require('../services/patient.service');

// Adding new patient in database
const createPatient = async (req, res) => {

    let patient = await patientService.findPatientByEmail(req.body);
    if (patient) {
        return res.status(400).send("Email already exist");
    }

    req.body.type = "Patient";
    req.body.createdAt = new Date();
    patient = await patientService.createPatient(req.body)

    return res.status(200).send(patient);
}


// Update a Patient
const updatePatient = async (req, res) => {
    const patient = await patientService.updatePatientById(req.params.patientId,req.body);

    if(!patient) {
        res.status(400).send("Patient not found");
    }

    res.send(patient).status(200);
}


// Getting a patient by id
const getPatientById = async (req,res) => {

    const patient = await patientService.getPatientById(req.params.patientId);

    if(!patient) {
        res.status(400).send("Patient not found");
    }

    res.send(patient).status(200)
}

// Getting all patients
const getAllPatients = async (req, res) => {

    return res.status(200).send(await patientService.getAllPatients(req.query))
}


// Delete a patient by id
const deletePatientById = () => {

}

const deleteAllPatients = async (req, res) => {

    await patientModel.deleteMany();
    return res.send(null).status(200)
}


module.exports = {
    createPatient,
    getPatientById,
    getAllPatients,
    updatePatient,
    deletePatientById,
    deleteAllPatients
}