const express = require('express');
const router = express.Router();
const donar = require('./donar.route');
const patient = require('./patient.route');


router.use('/patient', patient);
router.use('/donar', donar);

module.exports = router;