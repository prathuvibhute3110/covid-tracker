const express = require('express');
const router = express.Router();
const patientControllers = require('../controllers/patient.controller');

// Add patient
router
    .route('/')
    .post(patientControllers.createPatient)
    .get(patientControllers.getAllPatients)

// Get & update a patient by id
router
    .route('/:patientId')
    .get(patientControllers.getPatientById)
    .patch(patientControllers.updatePatient)

// // Delete a patient by id
// router
//     .route('/:patientId')
//     .delete(patientControllers.deletePatientById)

// Delete all patients
router.
    route('/')
    .delete(patientControllers.deleteAllPatients)

module.exports = router;