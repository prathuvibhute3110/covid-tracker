const express = require('express');
const router = express.Router();
const donarControllers = require('../controllers/donar.controller');

// Add patient
router
    .route('/')
    .post(donarControllers.createDonar)
    .get(donarControllers.getAllDonars)

// Get & update a patient by id
router
    .route('/:donarId')
    .get(donarControllers.getDonarById)
    .patch(donarControllers.updateDonar)

// // Delete a patient by id
// router
//     .route('/:patientId')
//     .delete(patientControllers.deletePatientById)

// Delete all patients
router.
    route('/')
    .delete(donarControllers.deleteAllDonars)

module.exports = router;