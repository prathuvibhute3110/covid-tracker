const patientModel = require('../models/patient.model');


const getAllPatients = async (query) => {

    return await patientModel.find();
}

const getPatientById = async (patientId) => {

    const patient = await patientModel.findById(patientId);

    if(!patient) {
        return null;
    }

    return patient
}

const createPatient = async (patient) => {

    return patientModel.create(patient)
}

const findPatientByEmail = async (patient) => {

    const data = await patientModel.find({ email: patient.email });
    return data[0];
}

const updatePatientById = async (patientId, updateBody) => {

    const patient = await patientModel.findById(patientId);

    if(!patient) {
        return null
    }

    Object.assign(patient,updateBody);
    await patient.save();
    return patient
}


module.exports = {
    createPatient,
    findPatientByEmail,
    getAllPatients,
    getPatientById,
    updatePatientById
}