const donarModel = require('../models/donar.model');


const getAllDonars = async (query) => {

    return await donarModel.find();
}

const getDonarById = async (donarId) => {

    const donar = await donarModel.findById(donarId);

    if (!donar) {
        return null;
    }

    return donar
}

const createDonar = async (donar) => {

    return donarModel.create(donar)
}

const findDonarByEmail = async (donar) => {

    const data = await donarModel.find({ email: donar.email });
    return data[0];
}

const updateDonarById = async (donarId, updateBody) => {

    const donar = await donarModel.findById(donarId);

    if (!donar) {
        return null
    }

    Object.assign(donar, updateBody);
    await donar.save();
    return donar
}


module.exports = {
    createDonar,
    findDonarByEmail,
    getAllDonars,
    getDonarById,
    updateDonarById
}