
const express = require('express');
const cors = require('cors');
const app = express();
const routes = require('./routes');
const bodyParser = require('body-parser')

const corsOption = {
    origin: ['http://localhost:3000', 'http://cotracker.netlify.app', ""],
    optionsSuccessStatus: 200
}

app.use(cors(corsOption));
app.use(bodyParser.json())
app.use('/', routes);

module.exports = app;