const mongoose = require('mongoose')

const donar = mongoose.Schema({
    name: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        trim: true
    },
    address: {
        type: String,
        trim: true
    },
    phoneNumber: {
        type: String,
        trim: true
    },
    bloodGroup: {
        type: String,
        trim: true
    },
    createdAt: {
        type: Date
    },
    type: {
        type: String,
        enum: ["Patient", "Donar"]
    }
})

module.exports = mongoose.model("Donar", donar);