import { Component } from "react";
import { connect } from "react-redux";

// Action
import getCovidHospitalData from '../../Actions/covidHospitalData';

// Component
import Footer from '../Footer';

// Assets
import titleIllustration from '../../assets/carecentreicon.gif';

class CovidCareCenter extends Component {

    constructor(props) {

        super(props);

        this.state = {

            hospitalList: null,
            selectHospital: "-- Please Select --"
            
        }
    }

    componentDidMount() {

        this.props.navStatus(0);
        var data = this.props.hospitalList;
        this.props.getCovidHospitalData();

        if(data != null) {
            this.setState({hospitalList: this.props.hospitalList});
        }
    }

    componentDidUpdate() {

        if(this.props.hospitalList != null && this.state.hospitalList == null) {
            this.setState({hospitalList: this.props.hospitalList});
        }
    }

    onHospitalSelect = (e) => {
        this.setState({selectHospital: e.target.value});
    }

    table() {

        // Hospital Change on Dropdown List
        return (
            <>
                {this.state.selectHospital !== "-- Please Select --" ?
                    
                    this.state.hospitalList.map((hospital,key) => {
                        if(this.state.selectHospital === hospital.name) {
                            return (
                                <tr>
                                    <td>{key+1}</td>
                                    <td>{hospital.name}</td>
                                    <td>{hospital.contact}</td>
                                    <td>{hospital.address}</td>
                                    <td className>
                                        <div>
                                            <div className={
                                                (hospital.without_oxygen[0] >= (hospital.without_oxygen[1]/2)) ?
                                                "green" :  (hospital.without_oxygen[1] - hospital.without_oxygen[0]) != 0 ?
                                                "yellow" :
                                                "red"
                                            }
                                            >
                                                {hospital.without_oxygen[0]}  
                                            </div>
                                            <div className="normal">/ {hospital.without_oxygen[1]}</div>
                                        </div>
                                    </td>
                                    <td >
                                        <div>
                                            <div className={
                                                 (hospital.without_oxygen[0] >= (hospital.without_oxygen[1]/2)) ?
                                                "green" :  (hospital.without_oxygen[1] - hospital.without_oxygen[0]) != 0 ?
                                                "yellow" :
                                                "red"
                                            }
                                            >
                                                {hospital.with_oxygen[0]}  
                                            </div>
                                            <div className="normal">/ {hospital.with_oxygen[1]}</div>
                                        </div>
                                    </td>
                                </tr>
                            )   
                        }
                      })
                    :
                    this.state.hospitalList.map((hospital,key) => {
                        return (
                            <tr>
                                <td>{key+1}</td>
                                <td>{hospital.name}</td>
                                <td>{hospital.contact}</td>
                                <td>{hospital.address}</td>
                                <td className>
                                    <div>
                                        <div className={
                                            (hospital.without_oxygen[0] >= (hospital.without_oxygen[1]/2)) ?
                                            "green" :  (hospital.without_oxygen[1] - hospital.without_oxygen[0]) != 0 ?
                                            "yellow" :
                                            "red"
                                        }
                                        >
                                            {hospital.without_oxygen[0]}  
                                        </div>
                                        <div className="normal">/ {hospital.without_oxygen[1]}</div>
                                    </div>
                                </td>
                                <td >
                                    <div>
                                        <div className={
                                             (hospital.without_oxygen[0] >= (hospital.without_oxygen[1]/2)) ?
                                            "green" :  (hospital.without_oxygen[1] - hospital.without_oxygen[0]) != 0 ?
                                            "yellow" :
                                            "red"
                                        }
                                        >
                                            {hospital.with_oxygen[0]}  
                                        </div>
                                        <div className="normal">/ {hospital.with_oxygen[1]}</div>
                                    </div>
                                </td>
                            </tr>
                    )})
                }
            </>
        );
    }

    render() {

        
        return (
            <div className="CovidCareCenter">
                <div className="CovidCareCenter_TitleBlock">
                    <img src={titleIllustration}/>
                    <div className="CovidCareCenter_Title">Covid Care Centres</div>
                </div>
                <div className="CovidCareCenter_Desc">* Information in this dashboard is subject to data filled in by hospitals themselves. *</div>
                
                <div className="CovidCareCenter_SearchBarTable">
                    
                    <div className="CovidCareCenter_SearchBarBlock">
                      {/*} <div className="SelectDistrict">
                            <label>District:</label>
                            <select>
                                <option selected>-- Please Select --</option>
                            </select>
                        </div> */}
                        <div className="SelectHospital">
                            <label>Hospital:</label>
                            <select onChange={(e) => this.onHospitalSelect(e)}> 
                            <option>-- Please Select --</option>
                                {this.props.hospitalList != null ?
                                 this.props.hospitalList.map((hospital,key) => {
                                     return <option>{hospital.name}</option>;
                                 }) :
                                     null
                                }
                            </select>
                        </div>
                        {/*<button className="CovidCareCenter_SearchBtn">Search</button> */}
                    </div>
                    
                    <div className="CovidCareCenter_Table">
                        <table cellSpacing={0} >
                            <tr>
                                <th>Sr No</th>
                                <th>Hospital Name</th>
                                <th>Contact</th>
                                <th>Address</th>
                                <th>Bed Without Oxyegn</th>
                                <th>Bed With Oxygen</th>
                            </tr>
                            
                            {/* Hospital Data Table */}
                            { this.state.hospitalList != null ?
                                 
                                this.table():
                                <tr>
                                    <td>0</td>
                                    <td>name</td>
                                    <td>contact</td>
                                    <td>address</td>
                                    <td>0/0</td>
                                    <td>0/0</td>
                                </tr>
                            }
                        </table>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}

const mapStateToProps = (state) => {

    if(state.hospitalData != null) {

        return {
            hospitalList: state.hospitalData
        };
    }
}

export default connect(mapStateToProps,
    {getCovidHospitalData}
)(CovidCareCenter);