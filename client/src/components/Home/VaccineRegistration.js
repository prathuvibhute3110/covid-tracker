import { Component } from "react";

// Component
import Footer from '../Footer';

// Assets
import element from '../../assets/element.svg';
import element1 from '../../assets/element_5.svg'
import illustration1 from '../../assets/vaccineIllustration1.gif';
import illsutration2 from '../../assets/vaccineIllustration2.gif';
import illsutration3 from '../../assets/vaccineIllustration3.gif';

class VaccineRegistration extends Component {


    impBlock2(subtitle, desc) {

        return (
            <div className="Vaccine_Block2">
                <div className="Vaccine_SubTitle2">{subtitle}</div>
                <div className="Vaccine_Block2Desc">{desc}</div>
            </div>
        );
    }

    render() {

        return (
            <div className="Vaccine">
                <div className="Vaccine_MainInfo">
                    <img src={element} />
                    <div className="Vaccine_MainInfoBlock">
                        <div className="Vaccine_MainInfoTitle">What is a Vaccine?</div>
                        <div className="Vaccine_MainInfoDesc1">
                            We’re protected from infectious disease by our immune
                            system, which destroys disease-causing germs – also known
                            as pathogens – when they invade the body. If our immune
                            system isn’t quick or strong enough to prevent pathogens
                            taking hold, then we get ill.
                        </div>
                        <div className="Vaccine_MainInfoDesc2">
                            We use vaccines to stop this from happening. A vaccine
                            provides a controlled exposure to a pathogen, training and
                            strengthening the immune system so it can fight that
                            disease quickly and effectively in future. By imitating an
                            infection, the vaccine protects us against the real thing.
                        </div>
                    </div>
                </div>
                <div className="Vaccine_Importance">
                    <div className="Vaccine_Block1">
                        <div className="Vaccine_ImgBlock">
                            <img src={illustration1} />
                        </div>
                        <div className="Vaccine_ImpBlock">
                            <div className="Vaccine_ImpBlockTitle">Why are vaccine important?</div>
                            <div className="Vaccine_Line"></div>
                            <div className="Vaccine_ImpBlockSubTitle1">They protect us from dangerous disease:</div>
                            <div className="Vaccine_ImpDesc">
                                In some regions or populations, dangerous diseases are constantly present (endemic). Examples include hepatitis B, cholera and polio. So long as these diseases are around, we need vaccines to bolster our immune systems and protect us from harm.
                            </div>
                        </div>
                    </div>
                    {this.impBlock2("They protect children and the elderly:", "Our immune systems are strongest in adulthood, meaning that young children and the elderly are particularly susceptible to dangerous infections. By strengthening our immune systems early and late on in life, vaccines bypass this risk.")}
                    {this.impBlock2("They protect the vulnerable: ", "If enough of a population is vaccinated, infections can’t spread from person to person, which means that everyone has a high level of protection – even those who don’t have immunity.This is known as herd protection (or herd immunity). It’s important because not everyone can be directly protected with vaccines – some people are unresponsive to them or have allergies or health conditions that prevent them from taking them. ")}
                    {this.impBlock2("They can help us control epidemics:", " In a world of denser cities, increased international travel, migration and ecological change, the ability of emerging infectious diseases (such as Ebola) to spread and cause devastation is increasing. Vaccines can be a key tool in managing this threat – but only if we have them ready for diseases when they appear.")}
                    {this.impBlock2("They can help limit drug resistance:", "Medicine relies on being able to treat infectious diseases with antimicrobial drugs, such as antibiotics, but overuse and misuse of these drugs is leading to infections becoming resistant to them. By preventing infections that would require drug treatments, vaccines reduce the opportunity for drug resistance to develop. ")}
                    {this.impBlock2("They are our most effective health intervention:", "Vaccines prevent an estimated 2–3 million deaths worldwide every year. But, a further 1.5 million lives could be saved annually with better global vaccine coverage.")}
                </div>
                <div className="Vaccine_VaccineWorkBlock">
                    <div className="Vaccine_WorkBlockInfo">
                        <div className="Vaccine_WorkBlockTitle">How does a vaccine work?</div>
                        <div className="Vaccine_Line"></div>
                        <div className="Vaccine_WorkDesc1">
                            Our immune system fights disease by distinguishing between things
                            that belong in our bodies and things that don’t, destroying the latter.
                            Unwanted foreign substances are identified by markers on their
                            surface called antigens.
                        </div>
                        <div className="Vaccine_WorkDesc2">
                            A vaccine works by exposing the immune system to the
                            antigens from a pathogen, something such as a virus or
                            bacterium that causes a certain disease. When your immune
                            cells encounter these antigens, they mount a response.
                            One cell type – B cells – start making antibodies,
                            which bind to the foreign substance, disable it and mark
                            it for destruction. Other immune cells, known as T cells,
                            attack and destroy cells of the body that have been
                            infected by the pathogen.
                        </div>
                        <div className="Vaccine_WorkDesc3">
                            At the same time, the body also produces long-lived types of white blood cell – 
                            called memory T cells and memory B cells – that remember the antigens that have 
                            just been encountered. If your immune system comes across the same antigens again, 
                            these memory cells allow you to mount a strong response against that specific pathogen 
                            very quickly, so you are much less likely to get ill.
                        </div>
                    </div>
                    <div className="Vaccine_WorkBlockImg">
                        <img src={illsutration2} />
                    </div>
                </div>
                <div className="Vaccine_RegisterCardBlock">
                    <img src={element1}/>
                    <div className="Vaccine_RegisterCard">
                        <div className="Vaccine_Empty">
                            <div className="Vaccine_RegisterTitle">
                                Register Yourself in Co-WIN
                            </div>
                            <div className="Vaccine_RegLine"></div>
                            <div className="Vaccine_CardBlock">
                                <a href="https://www.cowin.gov.in/" target="_blank" ><button className="Vaccine_RegButton">Resiter Here</button></a>
                                <div className="Vaccine_TagLine">
                                    <div className="TagLine1"># LARGEST</div>
                                    <div className="TagLine2">VACCINE</div>
                                    <div className="TagLine3">DRIVE</div>
                                </div>
                                <img src={illsutration3}/>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}

export default VaccineRegistration;