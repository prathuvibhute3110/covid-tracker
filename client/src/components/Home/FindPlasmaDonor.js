import { Component } from 'react';
import { connect } from 'react-redux';
import { URL } from '../../_Utils/Api';
// Action
import getCovidDonorData from '../../Actions/covidDonorData';


// Component
import Footer from '../Footer';

// Assets
import element from '../../assets/element.svg';
import searchAnimation from '../../assets/searchanimation.gif';
import axios from 'axios';

class FindPlasmaDonor extends Component {

    constructor(props) {

        super(props);

        this.state = {

            donarList: [],
            donorAddress: "",
        }
    }

    async getCovidDonorData() {

        await axios.get(`${URL}donar`).then((res) => {
            console.log(res.data);
            this.setState({ donarList: res.data })
        })
    }

    componentDidMount() {

        this.getCovidDonorData();
    }

    componentDidUpdate() {

        if (this.state.donarList == null) {
            this.getCovidDonorData();
        }
    }

    searchBar(donor) {

        if (donor !== undefined) {
            return donor.filter(
                (data) =>
                    data.address
                        .toLowerCase()
                        .indexOf(this.state.donorAddress.toLowerCase()) > -1
            );
        }
    }

    render() {

        return (
            <div className='DonorNearMe' style={{ backgroundColor: 'white' }}>
                <div className='DonorNearMe_Intro'>
                    <img src={element} />
                    <div className='DonorNearMe_IntroBlock'>
                        <div className='DonorNearMe_Title'>Plasma Donor Near Me</div>
                        <div className='DonorNearMe_Desc'>
                            This list auto generated list from our donor's Forms collected on this site.  All forms filled up by donor themselves and list generate auto with given information. They are unknown to us. We assume that all information filled by donor is correct and true as they accept such terms and condition. But we cannot confirm you that this information is true. We develop this website for social initiative in todays Covid-19 conditions.
                        </div>
                    </div>
                </div>
                <div className='DonorNearMe_DonorTable'>
                    <div className='DonorNearMe_SearchBarBlock'>
                        <img src={searchAnimation} />
                        <input className="LiveUpdate_SearchBar" type="search" placeholder="Enter Your Area" onChange={(e) => this.setState({ donorAddress: e.target.value })} />
                    </div>
                    <div className='DonorNearMe_Table'>
                        <table cellSpacing={0} >
                            <thead>
                                <tr>
                                    <th>Sr No</th>
                                    <th>Donor Name</th>
                                    <th>Contact</th>
                                    <th>Blood Group</th>
                                    <th>Address</th>
                                    <th>Registration Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.donarList !== 0 ?
                                    this.searchBar(this.state.donarList).map((donar, key) => {

                                        return (
                                            <tr key={key}>
                                                <td>{key + 1}</td>
                                                <td>{donar?.name}</td>
                                                <td>{donar?.phoneNumber}</td>
                                                <td>{donar?.bloodGroup}</td>
                                                <td>{donar?.address}</td>
                                                <td>{donar?.createdAt}</td>
                                            </tr>
                                        )
                                    }
                                    )
                                    :
                                    <tr>
                                        <td colSpan={6}>0</td>
                                    </tr>
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}

export default FindPlasmaDonor;