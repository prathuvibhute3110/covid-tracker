import { Component } from "react";
import { connect } from 'react-redux';

// Component
import Footer from '../Footer';

// Action
import getCovidData from '../../Actions/covidGlobalData';

// Assets
import element from '../../assets/element.svg';
import liveUpdate from '../../assets/COVIDLiveUpdate.svg'
import mask from '../../assets/humanmaskicon.gif';
import corona from '../../assets/corona.gif';
import heart from '../../assets/hearticon.gif';

class CovidLiveUpdate extends Component {

    constructor(props) {

        super(props);

        this.state = {

            totalInfected: 0,
            totalRecovered: 0,
            totalDeaths: 0,
            countryList: [],
            countryName: "",
        };
    }

    componentDidMount() {

        var data = this.props.covidData;
        var countryData = this.props.covidCountry;
        this.props.getCovidData();

        if(data != null) 
            this.setState({totalInfected: data.NewConfirmed, totalRecovered: data.NewRecovered, totalDeaths: data.NewDeaths,countryList: countryData})
    }

    componentDidUpdate() {

        var data = this.props.covidData;
        var countryData = this.props.covidCountry;
       
        if(this.state.totalInfected === 0)
            this.setState({totalInfected: data.NewConfirmed, totalRecovered: data.NewRecovered, totalDeaths: data.NewDeaths,countryList: countryData})   
    }

    onInputChange(e) {

        this.setState({countryName: e.target.value});
    }

    searchBar(country) {
        console.log(country)
        if (country !== undefined) {
          return country.filter(
            (data) =>
              data.Country
                .toLowerCase()
                .indexOf(this.state.countryName.toLowerCase()) > -1
          );
        }
    }

    render() {

        return (
            <div className="LiveUpdate">
                <div className="LiveUpdate_GlobalData">
                    <img src={element}/>
                    <div className="LiveUpdate_GlobalTitle">
                        <img src={liveUpdate}/>
                        COVID Live Update
                    </div>
                    <div className="LiveUpdate_DataBlock">
                        <div className="LiveUpdate_DataBlock1">
                            <div className="LiveUpdate_Cases">
                                <div className="LiveUpdate_Title">
                                    <img src={corona}/>
                                        Infected
                                </div>
                                <div className="LiveUpdate_Count">
                                    {this.state.totalInfected}
                                </div>
                            </div>
                            <div className="LiveUpdate_Death">
                            <div className="LiveUpdate_Title">
                                    <img src={heart}/>
                                        Death
                                </div>
                                <div className="LiveUpdate_Count">
                                    {this.state.totalDeaths}
                                </div>
                            </div>
                        </div>
                        <div className="LiveUpdate_DataBlock2">
                            <div className="LiveUpdate_Recovered">
                            <div className="LiveUpdate_Title">
                                    <img src={mask}/>
                                        Recovered
                                </div>
                                <div className="LiveUpdate_Count">
                                    {this.state.totalRecovered}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="LiveUpdate_Table">
                    <input className="LiveUpdate_SearchBar" type="search" placeholder="Search Country" onChange={(e) => this.onInputChange(e)}/>
                    <table cellSpacing={0} >
                        <tr>
                            <th>Sr No</th>
                            <th>Country Name</th>
                            <th>Total Cases</th>
                            <th>Total Recovered</th>
                            <th>Total Deaths</th>
                            <th>Total Vaccinated</th>
                        </tr>
                        {this.state.countryList.length !== 0 && this.state.countryList[0] !== undefined ?
                            this.searchBar(this.state.countryList).map((country,key) => {
                                return (
                                    <tr>
                                        <td>{key+1}</td>
                                        <td>{country.Country}</td>
                                        <td>{country.TotalConfirmed}</td>
                                        <td>{country.TotalRecovered}</td>
                                        <td>{country.TotalDeaths}</td>
                                        <td>80%</td>
                                    </tr>
                                );
                            }) :
                                    <tr>
                                        <td>0</td>
                                        <td>name</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0%</td>
                                    </tr>
                        }
                    </table>
                </div>
                <Footer />
            </div>
        );
    }
}

const mapStateToProps = (state) => {

    if(state.covidData !== null) 
        return {
            covidData: state.covidData.Global,
            covidCountry: state.covidData.Countries
        };
}

export default connect(mapStateToProps,
    {getCovidData}
)(CovidLiveUpdate);