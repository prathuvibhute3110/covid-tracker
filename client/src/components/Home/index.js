// importing dependencies
import { Component } from "react";
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

// Action
import addUserData from '../../Actions/addUsersData';

//import firebase from 'firebase';
import { collection, addDoc } from 'firebase/firestore/lite';
import db from '../Firebase/index';

// Path
import { COVIDUPDATE, DONORNEARME, COVIDCARECENTER, COVIDVACCINEREG } from '../_Utils/Path';
import { URL } from "../../_Utils/Api";

// Component
import Footer from "../Footer/index";

// Assets
import element from '../../assets/element.svg';
import img1 from '../../assets/COVIDLiveUpdate.svg';
import img2 from '../../assets/CovidCareCentres.svg';
import img3 from '../../assets/PlasmaDonorRegistration.svg';
import img4 from '../../assets/DonorNearYou.svg';
import img5 from '../../assets/PatientRegistration.svg';
import img6 from '../../assets/analytics_infected.svg';
import x from '../../assets/x.svg';

//import { HOME, INFORMATION, ABOUTUS, LOGIN, ANALYTICS, COVIDUPDATE, DONORNEARME, COVIDCARECENTER, COVIDVACCINEREG} from './_Utils/Path';


class Home extends Component {

    constructor(props) {

        super(props);

        this.state = {

            showPatientCard: false,
            showDonorCard: false,

            // User Info
            name: "",
            phoneNumber: "",
            address: "",
            email: "",
            bloodGroup: "",

            bloogGroups: [
                "Select Your Blood Group",
                "A +ve",
                "B +ve",
                "AB +ve",
                "O +ve",
                "A -ve",
                "B -ve",
                "AB -ve",
                "O -ve"
            ]
        }
    }

    componentDidMount() {

        this.props.navStatus(false);
    }

    addUser = (ref, title) => {

        let type = title === "Patient Registration" ? "Patient" : "Donor";

        let userData = {
            name: this.state.name,
            type: type,
            address: this.state.address,
            phoneNumber: this.state.phoneNumber,
            bloodGroup: this.state.bloodGroup,
            email: this.state.email,
            // loginStatus: false,
        }

        let promise = { ref, userData };

        this.props.addUserData(promise);

    }

    // Logical Section

    onNameChange = (e) => {

        this.setState({ name: e.target.value })
    }

    onEmailChange = (e) => {

        this.setState({ email: e.target.value })
    }

    onPhoneNumberChange = (e) => {

        let phoneNumber = e.target.value;

        let status = true;

        for (let i = 0; i <= phoneNumber.length; i++) {

            if ((phoneNumber.charAt(i) >= 'A' && phoneNumber.charAt(i) <= 'Z') || (phoneNumber.charAt(i) >= 'a' && phoneNumber.charAt(i) <= 'z')) {
                status = false;
                break;
            } else {
                status = true;
            }
        }

        if (status)
            this.setState({ phoneNumber: e.target.value })

    }

    onAddressChange = (e) => {

        this.setState({ address: e.target.value })
    }

    onbloodGroupChange = (e) => {

        this.setState({ bloodGroup: e.target.value })
    }

    showCardChange = (title) => {

        if (title === "Patient Registration")
            this.setState({ showPatientCard: !this.state.showPatientCard, name: "", address: "", phoneNumber: "", email: "" })
        else
            this.setState({ showDonorCard: !this.state.showDonorCard, name: "", address: "", phoneNumber: "", email: "" })

    }

    onFormBtnClick = (title) => {

        if (this.state.name.length !== 0 && this.state.email.length !== 0 && this.state.phoneNumber.length !== 0 && this.state.address.length !== 0) {

            this.addUser(collection(db, 'user'), title);

            if (title === "Patient Registration")
                this.setState({ showPatientCard: !this.state.showPatientCard })
            else
                this.setState({ showDonorCard: !this.state.showDonorCard })
        } else {
            alert("Please Fill Entire Fields");
        }
    }

    // UI Elements

    serviceCards(img, title, desc) {

        return (
            <div className='Home_Cards'
                onClick={() => this.showCardChange(title)}
            >
                <div className='Home_CardImage'>
                    <img src={img} />
                </div>
                <div className='Home_CardTitle'>{title}</div>
                <div className='Home_CardDesc' style={{ color: "#7D7987" }} >{desc}</div>
            </div>
        );
    }

    registrationCards() {

        var title = this.state.showPatientCard ? "Patient Registration" : "Donor Registration";

        return (
            <div className="Home_PopUpActive">
                <div className="Home_PopUpCard">
                    <div className="Home_PopUpTitleBlock">
                        <div className="Home_PopUpTitle">{title}</div>
                        <img src={x} onClick={() => this.showCardChange(title)} />
                    </div>
                    <div className="Home_FormBlock">
                        <div className="Home_FormComp">
                            <label>Name</label>
                            <input
                                value={this.state.name}
                                placeholder="Enter Your Full Name"
                                required
                                onChange={(e) => this.onNameChange(e)}
                            />
                        </div>
                        <div className="Home_FormComp">
                            <label>Email</label>
                            <input
                                type="email"
                                value={this.state.email}
                                placeholder="Enter Your Email Address"
                                required
                                onChange={(e) => this.onEmailChange(e)}
                            />
                        </div>
                        <div className="Home_FormComp">
                            <label>Address</label>
                            <input
                                value={this.state.address}
                                placeholder="Enter Your Full Address"
                                required
                                onChange={(e) => this.onAddressChange(e)}
                            />
                        </div>
                        <div className="Home_FormComp">
                            <label>Phone Number</label>
                            <input
                                placeholder="Enter Your Phone Number"
                                value={this.state.phoneNumber}
                                required
                                maxLength="10"
                                onKeyPress={(event) =>
                                    event.key === " " ? event.preventDefault() : true
                                }
                                onChange={(e) => this.onPhoneNumberChange(e)}
                            />
                        </div>
                        <div className="Home_FormComp">
                            <label>Select Blood Group</label>
                            <select required
                                onChange={(e) => this.onbloodGroupChange(e)}
                            >
                                {this.state.bloogGroups.map((bloodGroup, index) => <option key={index}>{bloodGroup}</option>)}
                            </select>
                        </div>
                    </div>
                    <button className="Home_FormBtn" onClick={() => this.onFormBtnClick(title)}>Sign Up</button>
                </div>
            </div>
        );
    }

    render() {

        return (
            <div className="Home">
                <div className="Home_Overview">
                    <img src={element} />
                    <div className="Home_Info">
                        <div className="Home_InfoTitle">Coronavirus COVID-19 Overview</div>
                        <div className="Home_InfoDesc1">Coronavirus disease (COVID-19) is an infectious disease
                            caused by a newly discovered coronavirus.
                            Most people
                            infected with the COVID-19 virus will experience mild to
                            moderate respiratory illness and recover without requiring
                            special treatment. Older people, and those with underlying
                            medical problems like cardiovascular disease, diabetes,
                            chronic respiratory disease, and cancer are more likely to
                            develop serious illness. The best way to prevent and slow
                            down transmission is to be well informed about the COVID-19 virus,
                            the disease it causes and how it spreads.
                        </div>
                        <div className="Home_InfoDesc2">
                            Protect yourself and others from infection by washing your hands or using an alcohol based rub frequently and not touching your face.
                            The COVID-19 virus spreads primarily through droplets of saliva or discharge from the nose when an infected person coughs or sneezes,
                            so it’s important that you also practice respiratory etiquette (for example, by coughing into a flexed elbow).
                        </div>
                    </div>
                </div>
                <div className="Home_OurService">
                    <div className="Home_ServiceTitle">
                        Our Services
                    </div>
                    <div className="Home_Line"></div>
                    <div className="Home_ServiceTagLine">We provide best information about Covid-19</div>
                    <div className="Home_ServiceCardBlock">
                        <div className="Home_ServiceBlock1">
                            <Link style={{ textDecoration: 'none' }} to="/liveupdate">{this.serviceCards(img1, "COVID Live Update", "Reported Cases and Deaths by Country or Territory.")}</Link>
                            {this.serviceCards(img2, "Patient Registration", "If you want to see plasma donors information.")}
                            {this.serviceCards(img3, "PlasmaDonor Registration", "Those who have recovered from COVID-19 can register themselves on an online portal for the benefit of others.")}
                        </div>
                        <div className="Home_ServiceBlock2">
                            <Link style={{ textDecoration: 'none' }} to="/donornearme">{this.serviceCards(img4, "Find a Plasma Donor Near You", "Here you only see donor's count not other details of donor.")}</Link>
                            <Link style={{ textDecoration: 'none' }} to="/covidcarecenter">{this.serviceCards(img5, "Covid Care Centres", "Hospital Wise Bed Availability Dashboard.")}</Link>
                            <Link style={{ textDecoration: 'none' }} to="covidvaccineregistration">{this.serviceCards(img6, "Covid Vaccine Registration", "Register Yourself on CoWIN.")}</Link>
                        </div>
                    </div>
                </div>
                {this.state.showPatientCard || this.state.showDonorCard ?
                    this.registrationCards() :
                    null
                }
                <Footer />
            </div>
        );
    }
}

export default connect(null, {
    addUserData
}
)(Home);