
import { initializeApp } from 'firebase/app';
import { getFirestore, collection, getDocs } from 'firebase/firestore/lite';


const firebaseConfig = {
    apiKey: "AIzaSyBPYq86hWgmhjyP6LF4yLCZ2FiuaqiGHXQ",
    authDomain: "co-tracker-9ad27.firebaseapp.com",
    projectId: "co-tracker-9ad27",
    storageBucket: "co-tracker-9ad27.appspot.com",
    messagingSenderId: "989406709890",
    appId: "1:989406709890:web:ac493f8eb582d5fa03cc9b",
    measurementId: "G-WMZ9HN0SLF"
};

// Initialize Firebase
var app = initializeApp(firebaseConfig);
var db = getFirestore(app);
  
export default db;
