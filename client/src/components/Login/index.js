// import dependencies
import { Component } from 'react';
import {Link} from 'react-router-dom';

// Utils
import {Home} from '../_Utils/Path';
import History from '../../_Utils/History';

class Login extends Component {

    constructor(props) {

        super(props);

        this.state = {
            email: "",
            otp: "",
            button: false,
        }
    }

    componentDidMount() {

        this.props.navStatus([false,true]);
    }

    onResentOtp = () => {

        if(this.state.button !== false)
            this.setState({button: !this.state.button,email: "",otp: ""})
    }

    onButtonClicked = () => {

        History.push('/')
        window.location.reload()
        this.setState({button: !this.state.button})
    }

    render() {

        return (
            <div className='Login'>
                <div className="Login_Card">
                    <div className='Login_Title'>LOGIN</div>
                    <div className='Login_Form'>
                        <input className='Login_Email' placeholder='Enter Your Email' value = {this.state.email} onChange={(e) => this.setState({email: e.target.value})}/>
                        <input className='Login_Otp' placeholder='Enter OTP' value = {this.state.opt} onChange={(e) => this.setState({otp: e.target.value})}/>
                        <button className='Login_SendOtp' onClick={() => this.onButtonClicked()}>{this.state.button ? "LOGIN" : "SEND OTP"}</button>
                    </div>
                    <div className='Login_ResentOtp' onClick={() => this.onResentOtp()} >Resent OTP</div>
                    <Link style={{ textDecoration: 'none' }} to={Home}>
                        <div className='Login_RegisterYourSelf'>
                                Register Your Self
                        </div>
                    </Link>
                </div>
            </div>
        );
    }
}

export default Login;