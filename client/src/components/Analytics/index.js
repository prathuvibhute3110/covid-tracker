// importing dependencies
import { Component } from 'react';
import { connect } from 'react-redux'; 
import {Pie} from 'react-chartjs-2';


// Action
import getCovidData from '../../Actions/covidGlobalData';

// Assets
import element from '../../assets/element.svg';
import infected from '../../assets/CovidVaccineRegistration.svg';
import recovered from '../../assets/analytics_recovered.svg';
import death from '../../assets/analytics_death.svg';
import donor from '../../assets/analytics_donor.svg';
import symptoms from '../../assets/covidSymptoms.svg';
import earth from '../../assets/earth_illustration.gif';
import Footer from '../Footer';

class Analytics extends Component {

    constructor(props) {

        super(props);

        this.state = {

            totalInfected: 0,
            totalRecovered: 0,
            totalDeaths: 0,
            countryName: "Global",
            countryList: []
        };
    }

    componentDidMount() {

        this.props.navStatus(false);
        var data = this.props.covidData;
        var countryData = this.props.covidCountry;
        this.props.getCovidData();

        if(data != null) 
            this.setState({totalInfected: data.NewConfirmed, totalRecovered: data.NewRecovered, totalDeaths: data.NewDeaths,countryList: countryData})
    }

    componentDidUpdate() {

        var data = this.props.covidData;
        var countryData = this.props.covidCountry;
        //this.props.getCovidData();
        if(this.state.totalInfected === 0)
            this.setState({totalInfected: data.NewConfirmed, totalRecovered: data.NewRecovered, totalDeaths: data.NewDeaths,countryList: countryData})   
    }

    globalCntCards(img,title,color,count) {

        return (
            <div className='Analytics_CntCards'>
                <div className='Analytics_CardTitleBlock'>
                    <div className='Analytics_CardImage'>
                        <img src={img} />
                    </div>
                    <div className='CardTitle'>{title}</div>
                </div>
                <div className='Analytics_Count' style={{backgroundImage: color, backgroundClip: "text", WebkitTextFillColor: "transparent"}}>{count}</div>
            </div>
        );
    }

    render() {

        var color={
            one: {
                backgroundColor: "rgba(255, 0, 0, 0.29)",
                color: "rgba(255, 0, 0, 1)",
                border: "2px solid rgba(255, 0, 0, 1)",
            },
            two: {
                backgroundColor: "rgba(0, 224, 255, 0.29)",
                color: "rgba(0, 209, 255, 1)",
                border: "2px solid rgba(0, 209, 255, 1)",
            },
            three: {
                backgroundColor: "rgba(189, 0, 255, 0.29)",
                color: "rgba(204, 0, 255, 1)",
                border: "2px solid rgba(204, 0, 255, 1)",
            },
            four: {
                backgroundColor: "rgba(255, 168, 0, 0.29)",
                color: "rgba(255, 138, 0, 1)",
                border: "2px solid rgba(255, 138, 0, 1)",
            },
            five: {
                backgroundColor: "rgba(22, 163, 28, 0.29)",
                color: "rgba(9, 131, 21, 1)",
                border: "2px solid rgba(9, 131, 21, 1)",
            }
        }

        var data= {
            labels: ['Infected','Recovered','Death'],
            datasets: [
                {
                    backgroundColor: ["rgba(0, 163, 255, 0.8)","rgba(36, 255, 0, 0.8)","rgba(255, 0, 0, 0.8)"],
                    hoverOffset: 3,
                    data: [this.state.totalInfected,this.state.totalRecovered,this.state.totalDeaths]}
                    
            ]
        }

        // sorting array
        var sortedArray = this.state.countryList;
        var newSortedCountry = null;

        if(sortedArray.length !== 0 && sortedArray[0] !== undefined) {

            for(var i = 0; i < sortedArray.length-1;i++) {
                for(var j = i+1; j < sortedArray.length;j++) {
                    var temp = null;
                    if(sortedArray[i].TotalConfirmed < sortedArray[j].TotalConfirmed) {
                        temp = sortedArray[i];
                        sortedArray[i] = sortedArray[j];
                        sortedArray[j] = temp
                    }
                }
            }
            newSortedCountry = [sortedArray[0],sortedArray[1],sortedArray[2],sortedArray[3],sortedArray[4]]
        }

        //console.log(sortedArray)

        return (
            <div className='Analytics'>
                <div className='Analytics_GlobalDataBlock'>
                    <div className='Analytics_TitleBlock'>
                        <img src={element} />
                        <div className='Analytics_Title'>
                            Analytics
                            <div className='Analytics_Line'></div>
                        </div>
                    </div>
                    <div className='Analytics_CountBlock'>
                        <div className='Analytics_CntBlock1'>
                            {this.globalCntCards(infected,"Infected","linear-gradient(180deg, #6DF8FA 0%, #4699C6 100%)",this.state.totalInfected)}
                            {this.globalCntCards(recovered,"Recovered","linear-gradient(180deg, #6EFB74 0%, #4BCD67 100%)",this.state.totalRecovered)}
                            {this.globalCntCards(death,"Death","linear-gradient(180deg, #FC6E6E 0%, #FF7F34 100%)",this.state.totalDeaths)}
                        </div>
                        <div className='Analytics_CntBlock2'>
                            <div className='Analytics_SymptomsBlock'>
                                <div className='Analytics_SymptomsImage'>
                                    <img src={symptoms} />
                                </div>
                                <div className='Analytics_SymptomsContent'>
                                    <div className='Analytics_Sym'>
                                        <div className='Sym1' style={color.one}>
                                            HEADACHE
                                        </div>
                                        <div className='Sym2' style={color.two}>
                                            FEVER
                                        </div>
                                    </div>
                                    <div className='Analytics_Sym'>
                                        <div className='Sym3' style={color.three}>
                                            COUGH
                                        </div>
                                    </div>
                                    <div className='Analytics_Sym'>
                                        <div className='Sym4' style={color.four}>
                                            BREATH
                                        </div>
                                        <div className='Sym5' style={color.five}>
                                            FATIGUE
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {this.globalCntCards(donor,"Donor","linear-gradient(180deg, #BB6BFC 0%, #5B4DF6 100%)",90)}
                        </div>
                    </div>
                </div>
                <div className='Analytics_GraphTable'>
                    <div className='Analytics_Graph'>
                        <div className='Analytics_GraphTitle'>Update Case Statistics</div>
                        <div className='Analytics_PieChart'>
                            <Pie 
                                data={data}
                                options={{
                                    title:{
                                      display:true,
                                      text:'Average Rainfall per month',
                                      fontSize:20
                                    },
                                    legend:{
                                      display:true,
                                      position:'right'
                                    }
                                }}
                            />
                        </div>
                        <div className='Analytics_GraphImage'>
                            <img src={earth}/>
                            {this.state.countryName} Covid Statistics
                        </div>
                    </div>
                    <div className='Analytics_Table'>
                        <div className='Analytics_TableTitle'>
                            Top 5 Countries Stream
                        </div>
                        <div className='Analytics_TableData'>
                            <table>
                                <tr>
                                    <th>Country</th>
                                    <th>Total Cases</th>
                                    <th>Total Recovered</th>
                                    <th>Total Deaths</th>
                                </tr>
                                {sortedArray.length !== 0 && sortedArray[0] !== undefined ?
                                    newSortedCountry.map((country,key) => {
                                        console.log("Data",country)
                                        return (
                                            <tr  onClick={() => {
                                                this.setState({totalInfected: country.TotalConfirmed,
                                                    totalRecovered: country.TotalRecovered,
                                                    totalDeaths: country.TotalDeaths,
                                                    countryName: country.Country
                                                })
                                            }}>
                                                <td><img src={`https://www.countryflagicons.com/SHINY/64/${country.CountryCode}.png`} /></td>
                                                <td>{country.TotalConfirmed}</td>
                                                <td>{country.TotalRecovered}</td>
                                                <td>{country.TotalDeaths}</td>
                                            </tr>
                                        );
                                    }) :
                                    
                                            <tr>
                                                <td>name</td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>0</td>
                                            </tr>
                                }
                            </table>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}

const mapStateToProps = (state) => {


    if(state.covidData !== null) 
        return {
            covidData: state.covidData.Global,
            covidCountry: state.covidData.Countries
        };
}

export default connect(mapStateToProps,{
    getCovidData
})
(Analytics);