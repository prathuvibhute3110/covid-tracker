// import dependencies
import { Component } from 'react';
import { Link } from 'react-router-dom';



// Routes
import { HOME, INFORMATION, ABOUTUS, ANALYTICS, LOGIN } from '../../_Utils/Routes';

class Navbar extends Component {

    constructor(props) {

        super(props);

        this.state = {

            nav: [true, false, false, false, false],
        }
    }

    onPageChange = (page) => {

        if (page === "home")
            this.setState({ nav: [true, false, false, false, false] });
        else if (page === "info")
            this.setState({ nav: [false, true, false, false, false] });
        else if (page === "analytics")
            this.setState({ nav: [false, false, true, false, false] });
        else if (page === "aboutus")
            this.setState({ nav: [false, false, false, true, false] });
        else
            this.setState({ nav: [false, false, false, false, true] });
    }

    render() {

        console.log(this.props.navStatus);
        let navStatus = this.state.nav;
        console.log(navStatus)

        return (
            <div className='Navbar'>
                <div 
                    className={this.props.navStatus === 0? 
                                "Navbar_Covidcarecenter" :
                            navStatus[4] ?  
                                "Navbar_LActiveTitle" :
                            navStatus[3] ?
                                "Navbar_ActiveTitle":
                                "Navbar_InActiveTitle"
                            }
                >
                    Cotracker
                </div>
                <div className={this.props.navStatus === 0? 
                            "Navbar_CovidcarecenterContent" 
                            :'Navbar_Contents'}
                >
                    <Link
                        to={HOME}
                        className={this.props.navStatus === 0 ? 
                            "Navbar_CovidLink Navbar_LinkActive" :
                                navStatus[0] ? 
                                    "Navbar_LinkActive" :
                                navStatus[4] ?
                                    "Navbar_LLinkInActive":
                                    "Navbar_LinkInActive"
                        }
                        onClick={() => this.onPageChange("home")}
                    >
                        Home
                    </Link>
                    <Link 
                        to={INFORMATION} 
                        className={this.props.navStatus === 0 ? 
                            "Navbar_CovidLink Navbar_LinkInActive" :
                            navStatus[1] ? 
                            "Navbar_LinkActive" :
                            navStatus[4] ?
                                    "Navbar_LLinkInActive":
                                    "Navbar_LinkInActive"
                        }
                        onClick={() => this.onPageChange("info")} 
                    >
                        Information
                    </Link>
                    <Link 
                        to={ANALYTICS} 
                        className={this.props.navStatus === 0 ? 
                            "Navbar_CovidLink Navbar_LinkInActive" :
                            navStatus[2] ? 
                            "Navbar_LinkActive" :
                            navStatus[4] ?
                                    "Navbar_LLinkInActive":
                                    "Navbar_LinkInActive"
                        }
                        onClick={() => this.onPageChange("analytics")}
                    >
                        Analytics
                    </Link>
                    <Link 
                        to={ABOUTUS} 
                        className={this.props.navStatus === 0 ? 
                            "Navbar_CovidLink Navbar_LinkInActive" :
                            navStatus[3] ? 
                            "Navbar_LinkActive" :
                            navStatus[4] ?
                                    "Navbar_LLinkInActive":
                                    "Navbar_LinkInActive"
                        }
                        onClick={() => this.onPageChange("aboutus")}
                    >
                        AboutUs
                    </Link>
                    <Link 
                        to={LOGIN} 
                        className={this.props.navStatus === 0 ? 
                            "Navbar_CovidLink Navbar_LinkInActive" :
                            navStatus[4] ? 
                            "Navbar_LLinkActive" :
                            "Navbar_LinkInActive"
                        }
                        onClick={() => this.onPageChange("login")}
                    >
                        Login
                    </Link>
                </div>
            </div>
        );
    }
}

export default Navbar;