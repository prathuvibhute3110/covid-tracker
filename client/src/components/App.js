// import Dependencies
import React from 'react';
import { BrowserRouter} from 'react-router-dom'

import history from '../_Utils/History';

// Components
import RouterComponent from './RouterComponent'; 

class App extends React.Component {

    render() {
        return (
            <BrowserRouter  history={history}>
                <RouterComponent />
            </BrowserRouter>
        );
    }
}

export default App;