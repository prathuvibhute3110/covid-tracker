import { Component } from "react";

// Assets
import footerelement from '../../assets/element.svg';
import footerelement1 from '../../assets/element_1.svg';

export default class Footer extends Component {

    footerSubBlock(title,array) {
        var index = 0
        return (
            <div className="Footer_Points">
                <div className="Footer_PointsTitle">
                    {title}
                </div>
                <div className="Footer_PointsDesc">
                    {array.map((value) => {
                       
                        index = index + 1
                        return <div key = {index}className="points">{value}</div>
                    })}
                </div>
            </div>
        );
    }

    render() {

        return (
            <div className="Footer" >
                <div className="Footer_MainElement">
                    <img src={footerelement}/>
                </div>
                <div className="Footer_Block">
                    <div className="Footer_InfoBlock">
                        <div className="Footer_MainInfo">
                            <div className="Footer_CompName">COTRACKER</div>
                            <div className="Footer_CompDesc">COTRACKER provides best covid information for everyone</div>
                            <div className="Footer_CompCopyRight">©COTRACKER PTY LTD 2021. All rights reserved.</div>
                        </div>
                        {this.footerSubBlock("Compony",["Abouts Us"])}
                        {this.footerSubBlock("Region",["Pune"])}
                        {this.footerSubBlock("Help",["Help Center","Contact Support","Instructions","How it works"])}
                    </div>
                    <div className="Footer_ImageBlock">
                        <img src={footerelement1}/>
                    </div>
                </div>
            </div>
        );
    }
}

