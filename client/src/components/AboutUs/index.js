// import Dependencies
import { Component } from 'react';

// Assets
import illustration1 from '../../assets/aboutus_illustration1.gif';
import illustration2 from '../../assets/aboutus_illustration2.gif';
import dash1 from '../../assets/dash_1.svg';
import dash2 from '../../assets/dash_2.svg';


class AboutUs extends Component {

    componentDidMount() {

        this.props.navStatus(false);
    }

    render() {

        return (
            <div className='AboutUs'>
                <div className='AboutUs_CreatorInfoBlock'>
                    <div className='AboutUs_Creator1'>
                        <img src={illustration1} />
                        <div className='AboutUs_CreatorName'>Mr. Aditya Sayaji Dhamal</div>
                        <div className='AboutUs_Line'></div>
                        <div className='AboutUs_ProjectRole'>Web Developer</div>
                        <div className='AboutUs_CollegeDesc'>
                            I’m Aditya Sayaji Dhamal student of <br />
                            JAIKRANTI COLLEGE OF COMPUTER SCIENCE AND MANAGEMENT STUDIES <br />
                            (B.sc (COMPUTER SCIENCE) DEPARTMENT) <br />
                        </div> 
                        <img src={dash1}/>
                        <div className='AboutUs_Knowledge'>
                            React Developer<br />
                            Flutter Developer<br />
                            Ui/Ux Designer<br />
                        </div>
                    </div>
                    <div className='AboutUs_Creator2'>
                        <img src={illustration2} />
                        <div className='AboutUs_CreatorName'>Mr. Prathamesh Revan Vibhute</div>
                        <div className='AboutUs_Line'></div>
                        <div className='AboutUs_ProjectRole'>Web Developer</div>
                        <div className='AboutUs_CollegeDesc'>
                            I’m Prathamesh Revan Vibhute student of <br />
                            JAIKRANTI COLLEGE OF COMPUTER SCIENCE AND MANAGEMENT STUDIES <br />
                            (B.sc (COMPUTER SCIENCE) DEPARTMENT) <br />
                        </div>
                        <img src={dash2}/>
                        <div className='AboutUs_Knowledge'>
                            React Developer<br />
                            Flutter Developer<br />
                            Ui/Ux Designer<br />
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default AboutUs;