// import dependencies
import { Component } from 'react';
import { BrowserRouter , Route , Routes , Link} from 'react-router-dom';

// history
import history from "../_Utils/History";


// Component
import _AboutUs from './AboutUs/index';
import _Analytics from './Analytics/index';
import _Home from './Home/index';
import _Information from './Information/index';
import _Login from './Login/index';
import Navbar from './Navbar';
import _VaccineReg from './Home/VaccineRegistration';
import _PlasmaDonor from './Home/FindPlasmaDonor';
import _CareCenter from './Home/CovidCareCenter';
import _LiveUpdate from './Home/CovidLiveUpdate';

// Assets
import loader from '../assets/loader.gif';

import { HOME, INFORMATION, ABOUTUS, LOGIN, ANALYTICS, COVIDUPDATE, DONORNEARME, COVIDCARECENTER, COVIDVACCINEREG} from '../_Utils/Routes';
import { toHaveFormValues } from '@testing-library/jest-dom/dist/matchers';

class RouterComponent extends Component {

    constructor(props) {

        super(props);

        this.state = {
            load: false,
            navStatus: false,
            bgStatus: false,

            userLoginData: {
                email: "prathuvibhute3110@gmail.com",
                loginStatus: false,
            }
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({ load: true });
        }, 3000);
    }

    navStatus = (value) => {

        console.log("Value: ",value)
        if(this.state.navStatus)
            this.setState({navStatus: value, bgStatus: !value[1]})
        else
            this.setState({navStatus: value, bgStatus: !value[1]})
    }

    render() {

        return (
            <> {
                this.state.load ? (
                <div className= {this.state.bgStatus? 'App':'App WithBackground'}>
                    <Navbar navStatus= {this.state.navStatus}/>
                    <div className="Component_Container">
                        <Routes >
                            <Route exact path={ABOUTUS} element={<_AboutUs navStatus= {this.navStatus}/> } />
                            <Route exact path={ANALYTICS} element={<_Analytics navStatus= {this.navStatus} />} />
                            <Route exact path={INFORMATION} element={<_Information navStatus= {this.navStatus} />} />
                            <Route exact path={LOGIN} element={<_Login navStatus= {this.navStatus} userLoginData={this.state.userLoginData}/>} />
                            <Route exact path={HOME} element={<_Home navStatus= {this.navStatus} />} /> 
                            <Route exact path="/liveupdate" element={<_LiveUpdate/>}/>
                            <Route exact path="/donornearme" element={<_PlasmaDonor/>}/>
                            <Route exact path="/covidcarecenter" element={<_CareCenter navStatus= {this.navStatus} />}/>
                            <Route exact path="/covidvaccineregistration" element={<_VaccineReg/>}/> 
                        </Routes>
                    </div>
                </div>
                ) :
                (
                <div className="loader">
                    <img src={loader} alt="elemant" />
                </div>
                )
            }
            </>
            
        );
    }
}

export default RouterComponent;