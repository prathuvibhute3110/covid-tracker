// import dependencies
import { Component } from 'react';

// Component
import Footer from '../Footer';

// Assets
import illustration1 from '../../assets/infoIllustration1.gif';
import illustration3 from '../../assets/infoIllustration3.gif';
import illustration4 from '../../assets/infoIllustration4.gif';
import illustration5 from '../../assets/infoIllustration5.gif';
import illustration6 from '../../assets/infoIllustration6.gif';
import illustration7 from '../../assets/infoIllustration7.gif';
import illustration8 from '../../assets/infoIllustration8.gif';
import comeFrom from '../../assets/CoronavirusComeFrom.svg';
import infoImage3 from '../../assets/informationImage3.svg';
import infoImage4 from '../../assets/informationImage4.svg';
import infoImage5 from '../../assets/informationImage5.svg';
import Circle1 from '../../assets/Circle1.svg';


class Information extends Component {

    componentDidMount() {

        this.props.navStatus(false);
    }

    component1() {

        return (
            <div className='Information_Component1'>
                <div className='Information_Comp1Block1'>
                    <div className='Information_Comp1Title'>Coronavirus History</div>
                    <div className='Information_Comp1Desc1'>
                        Coronaviruses are a big family of different viruses. Some of them cause the common cold in people. Others infect animals, including bats, camels, and cattle. But how did SARS-CoV-2, the new coronavirus that causes COVID-19, come into being?
                    </div>
                    <div className='Information_Comp1Desc2'>
                        Here’s what we know about the virus that was first detected in Wuhan, China, in late 2019 and has set off a global pandemic.
                    </div>
                </div>
                <div className='Information_Comp1Block2'>
                    <img src={illustration1} />
                </div>
            </div>
        );
    }

    component2() {

        return (
            <div className='Information_Component2'>
                <div className='Information_Comp2Container1'>
                    <div className='Information_Comp2Block1'>
                        <img src={comeFrom} />
                    </div>
                    <div className='Information_Comp2Block2'>
                        <div className='Information_Comp2Title'>Where Did The Coronavirus Come From?</div>
                        <div className='Information_Comp2Line'></div>
                        <div className='Information_Comp2Desc1'>
                            Experts say SARS-CoV-2 originated in bats. That’s also how the coronaviruses behind Middle East respiratory syndrome (MERS) and severe acute respiratory syndrome (SARS) got started.
                        </div>
                    </div>
                </div>
                <div className='Information_Comp2Container2'>
                    <div className='Information_Comp2Desc2'>
                        SARS-CoV-2 made the jump to humans at one of Wuhan’s open-air “wet markets.” They’re where customers buy fresh meat and fish, including animals that are killed on the spot.
                    </div>
                    <div className='Information_Comp2Desc3'>
                        Some wet markets sell wild or banned species like cobras, wild boars, and raccoon dogs. Crowded conditions can let viruses from different animals swap genes. Sometimes the virus changes so much it can start to infect and spread among people.
                    </div>
                    <div className='Information_Comp2Desc4'>
                        Still, the Wuhan market didn’t sell bats at the time of the outbreak. That’s why early suspicion also fell on pangolins, also called scaly anteaters, which are sold illegally in some markets in China. Some coronaviruses that infect pangolins are similar to SARS-CoV-2.
                    </div>
                    <div className='Information_Comp2Desc5'>
                        As SARS-CoV-2 spread both inside and outside China, it infected people who have had no direct contact with animals. That meant the virus is transmitted from one human to another. It’s now spreading in the U.S. and around the globe, meaning that people are unwittingly catching and passing on the coronavirus. This growing worldwide transmission is what is now a pandemic.
                    </div>
                </div>
            </div>
        );
    }

    component3() {

        return (
            <div className='Information_Component3'>
                <div className='Information_Comp3Block1'>
                    <div className='Information_Comp3Title'>Coronavirus Evolution</div>
                    <div className='Information_Comp3Line'></div>
                    <div className='Information_Comp3Desc1'>
                        Scientists first identified a human coronavirus in 1965. It caused a
                        common cold. Later that decade, researchers found a group of
                        similar human and animal viruses and named them after their
                        crown-like appearance.
                    </div>
                    <div className='Information_Comp3Desc2'>
                        Seven coronaviruses can infect humans. The one that causes SARS
                        emerged in southern China in 2002 and quickly spread to 28 other
                        countries. More than 8,000 people were infected by July 2003, and
                        774 died. A small outbreak in 2004 involved only four more cases.
                        This coronavirus causes fever, headache, and respiratory problems
                        such as cough and shortness of breath.
                    </div>
                    <div className='Information_Comp3Desc3'>
                        MERS started in Saudi Arabia in 2012. Almost all of the nearly 2,500
                        cases have been in people who live in or travel to the Middle East.
                        This coronavirus is less contagious than its SARS cousin but more deadly
                        , killing 858 people. It has the same respiratory symptoms but can also
                        cause kidney failure.
                    </div>
                </div>
                <div className='Information_Comp3Block2'>
                    <img src={infoImage3} />
                </div>
            </div>
        );
    }

    component4() {

        return (
            <div className='Information_Component4'>
                <div className='Information_Comp4Container1'>
                    <div className='Information_Comp4Block1'>
                        <img src={infoImage4} />
                    </div>
                    <div className='Information_Comp4Block2'>
                        <div className='Information_Comp4Title'>
                            Varients Of Coronavirus
                        </div>
                        <div className='Information_Comp4Line'></div>
                        <div className='Information_Comp4SubTitle1'>What is a COVID-19 Variant?</div>
                        <div className='Information_Comp4Desc1'>
                            Viruses are always changing, and that can cause a new variant, or strain, of a virus to form. A variant usually doesn’t affect how the virus works. But sometimes they make it act in different ways.
                        </div>
                        <div className='Information_Comp4Desc2'>
                            Scientists around the world are tracking changes in the virus that causes COVID-19. Their research is helping experts understand whether certain COVID variants spread faster than others, how they might affect your health, and how effective different vaccines might be against them.
                        </div>
                    </div>
                </div>
                <div className='Information_Comp4Container2'>
                    <div className='Information_Comp4Block3'>
                        <div className='Information_Comp4SubTitle2'>How Many Coronavirus Are There?</div>
                        <div className='Information_Comp4Desc3'>
                            Coronaviruses didn’t just pop up recently. They’re a large family of viruses that have been around for a long time. Many of them can cause a variety of illnesses, from a mild cough to severe respiratory illnesses.
                        </div>
                        <div className='Information_Comp4Desc4'>
                            The new (or “novel”) coronavirus that causes COVID-19 is one of several known to infect humans. It’s probably been around for some time in animals. Sometimes, a virus in animals crosses over into people. That’s what scientists think happened here. So this virus isn’t new to the world, but it is new to humans. When scientists found out that it was making people sick in 2019, they named it as a novel coronavirus.
                        </div>
                    </div>
                    <div className='Information_Comp4Block4'>
                        <div className="Information_Comp4SubTitle3">
                            Human Coronavirus Types
                        </div>
                        <div className='Information_Comp4Points'>
                            <ul>
                                <li>-229E (alpha)</li>
                                <li> -NL63 (alpha)</li>
                                <li> -OC43 (beta)</li>
                                <li> -HKU1 (beta</li>
                                <li> -MERS-CoV, a beta virus that causes Middle East respiratory syndrome (MERS)</li>
                                <li> -SARS-CoV, a beta virus that causes severe acute respiratory syndrome (SARS)</li>
                                <li> -SARS-CoV-2, which causes COVID-19</li>
                            </ul>
                        </div>
                    </div>
                    <div className='Information_Comp4Block5'>
                        <div className='Information_Comp4SubTitle4'>
                            How Do Variants Happen?
                        </div>
                        <div className='Information_Comp4Desc5'>
                            Coronaviruses have all their genetic material in something called RNA (ribonucleic acid). RNA has some similarities to DNA, but they aren’t the same.
                        </div>
                        <div className='Information_Comp4Desc6'>
                            When viruses infect you, they attach to your cells, get inside them, and make copies of their RNA, which helps them spread. If there’s a copying mistake, the RNA gets changed. Scientists call those changes mutations.
                        </div>
                        <div className='Information_Comp4Desc7'>
                            These changes happen randomly and by accident. It’s a normal part of what happens to viruses as they multiply and spread. If a virus has a random change that makes it easier to infect people and it spreads, that variant will become more common.
                        </div>
                        <div className='Information_Comp4Desc8'>
                            Because the changes are random, they may make little to no difference in a person’s health. Other times, they may cause disease. For example, one reason you need a flu shot every year is because influenza viruses change from year to year. This year’s flu virus probably isn’t exactly the same one that circulated last year.
                        </div>
                        <div className='Information_Comp4Desc9'>
                            The bottom line is that all viruses, including coronaviruses, can change over time.
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    component5() {

        return (
            <div className='Information_Component5'>
                <div className='Information_Comp5Container1'>
                    <div className='Information_Comp5Block1'>
                        <div className='Information_Comp5Title'>Symptoms of COVID-19</div>
                        <div className='Information_Comp5Line'></div>
                        <div className='Information_Comp5Desc1'>
                            COVID-19 affects different people in different ways. Most infected
                            people will develop mild to moderate illness and recover without
                            hospitalization.
                        </div>
                        <div className='Information_Comp5Symptoms'>
                            <div className='Information_Comp5SubTitle'>Most Common Symptoms:</div>
                            <ul>
                                <li>Fever</li>
                                <li>Dry cough</li>
                                <li>Tiredness</li>
                            </ul>
                        </div>
                        <div className='Information_Comp5Symptoms'>
                            <div className='Information_Comp5SubTitle'>Less Common Symptoms:</div>
                            <ul>
                                <li>Aches and Pains</li>
                                <li>Sore throat</li>
                                <li>Diarrhoea</li>
                                <li>Conjuctivities</li>
                                <li>Headache</li>
                                <li>Loss of taste or smell</li>
                                <li>A rash on skin, or discolouration of figures or toes</li>
                            </ul>
                        </div>
                        <div className='Information_Comp5Symptoms'>
                            <div className="Information_Comp5SubTitle">
                                Serious Symptoms:
                            </div>
                            <ul>
                                <li>Difficulty in breathing or shortness of breath</li>
                                <li>Chest pain or pressure</li>
                                <li>Loss of speech or movement</li>
                            </ul>
                        </div>
                    </div>
                    <div className='Information_Comp5Block2'>
                        <img src={infoImage5} />
                    </div>
                </div>
                <div className='Information_Comp5Container2'>
                    Seek immediate medical attention if you have serious symptoms. Always call before visiting your doctor or health facility.People with mild symptoms who are otherwise healthy should manage their symptoms at home.
                    On average it takes 5–6 days from when someone is infected with the virus for symptoms to show, however it can take up to 14 days.
                </div>
            </div>
        )
    }

    component6() {

        return (
            <div className='Information_Component6'>
                <div className='Information_Comp6Container1'>
                    <div className='Information_Comp6Block1'>
                        <img className="backgroundcircle" src={Circle1} />
                    </div>
                    <div className='Information_Comp6Block2'>
                        <div className='Information_Comp6Title'>How To Protect Yourself & Others</div>
                        <div className='Information_Comp6Line'></div>
                        <div className='Information_Comp6Desc1'>
                            Protect yourself and others around you by knowing the facts
                            and taking appropriate precautions.
                            Follow advice provided by your local health authority.
                        </div>
                    </div>
                </div>
                <div className='Information_Comp6Container2'>
                    <div className='Information_Comp6SubTitle'>
                        To prevent the spread of COVID-19
                    </div>
                    <ul>
                        <li>Clean your hands often. Use soap and water, or an alcohol-based hand rub.</li>
                        <li>Maintain a safe distance from anyone who is coughing or sneezing.</li>
                        <li>Wear a mask when physical distancing is not possible.</li>
                        <li>Don’t touch your eyes, nose or mouth.</li>
                        <li>Cover your nose and mouth with your bent elbow or a tissue when you cough or sneeze.</li>
                        <li>Stay home if you feel unwell.</li>
                        <li>If you have a fever, cough and difficulty breathing, seek medical attention.</li>
                    </ul>
                </div>
            </div>
        );
    }

    precautions(cnt, img, title,precautions) {

        var index = 0
        var color = ["linear-gradient(180deg, #3ACAF5 2.4%, #63DAFE 30.52%, #63DAFE 100%)",
            "linear-gradient(180deg, #2452F4 0%, #0B278B 21.67%, #0B278B 100%)",
            "linear-gradient(180deg, #3655C6 0%, #0075FF 20.62%, #0075FF 100%)",
            "linear-gradient(180deg, #4494F1 5%, #78AAE4 19.06%, #78AAE4 100%)",
            "linear-gradient(180deg, #73D3F0 0%, #637CFD 15.94%, #637CFD 100%)",
            "linear-gradient(180deg, #63DAFE 0%, #0094C1 17.5%, #0094C1 100%, #0094C1 100%)"
        ]

        return (
            <div className='Information_Precaution'>
                <div className='Information_PrecautionContainer'
                    style={{ background: cnt === 1 ? color[0] : 
                            cnt === 2 ? color[1] : 
                            cnt === 3 ? color[2] : 
                            cnt === 4 ? color[3] : 
                            cnt === 5 ? color[4] : 
                            color[5]
                    }}
                >
                <div className='Information_PrecautionTitle'>
                    {title}
                </div>
                <div className='Information_PrecautionLine'></div>
                <div className='Information_PrecautionInfo'>
                    <div className='Information_PrecautionImage'>
                        <img src={img} />
                    </div>
                    <div className='Information_PrecautionDesc'>
                        <ul>
                            {precautions.map((desc) => {
                                index = index + 1
                                return <li key={index}>{desc}</li>
                            })}
                        </ul>
                    </div>
                </div>
            </div>
            </div >
        );
    }

    render() {

        var precautions= [[
            "Everyone 2 years and older should wear masks in public.",
            "Masks should be worn in addition to staying at least 6 feet apart, especially around people who don’t live with you.",
            "If someone in your household is infected, people in the household should take precautions including wearing masks to avoid spread to others.",
            "Wash your hands or use hand sanitizer before putting on your mask.",
            "Wear your mask over your nose and mouth and secure it under your chin."
        ],[
            "Inside your home: Avoid close contact with people who are sick.",
            "If possible, maintain 6 feet between the person who is sick and other household members.",
            "Outside your home: Put 6 feet of distance between yourself and people who don’t live in your household.",
            "Remember that some people without symptoms may be able to spread virus.",
            "Stay at least 6 feet (about 2 arm lengths) from other people.",
            "Keeping distance from others is especially important for people who are at higher risk of getting very sick."
        ],[
            "Authorized COVID-19 vaccines can help protect you from COVID-19.",
            "You should get a COVID-19 vaccine when it is available to you.",
            "Once you are fully vaccinated, you may be able to start doing some things that you had stopped doing because of the pandemic."
        ],[
            "Being in crowds like in restaurants, bars, fitness centers, or movie theaters puts you at higher risk for COVID-19.",
            "Avoid indoor spaces that do not offer fresh air from the outdoors as much as possible.",
            "If indoors, bring in fresh air by opening windows and doors, if possible."
        ],[
            "Wash your hands often with soap and water for at least 20 seconds especially after you have been in a public place, or after blowing your nose, coughing, or sneezing.",
            "Before eating or preparing food, Before touching your face",
            "After using the restroom, After leaving a public place",
            "After blowing your nose, coughing, or sneezing",
            "After handling your mask, After changing a diaper",
            "After caring for someone sick, After touching animals or pets"
        ],[
            "Clean high touch surfaces daily. This includes tables, doorknobs, light switches, countertops, handles, desks, phones, keyboards, toilets, faucets, and sinks.",
            "If someone is sick or has tested positive for COVID-19, disinfect frequently touched surfaces. Use a household disinfectant product from EPA’s List N: Disinfectants for Coronavirus (COVID-19)external icon according to manufacturer’s labeled directions.",
            "If surfaces are dirty, clean them using detergent or soap and water prior to disinfection."
        ]]

        return (
            <div className='Information'>
                {this.component1()}
                {this.component2()}
                {this.component3()}
                {this.component4()}
                {this.component5()}
                {this.component6()}
                {this.precautions(1, illustration3, "Wear a Mask",precautions[0])}
                {this.precautions(2, illustration4, "Stay 6 feet away from others",precautions[1])}
                {this.precautions(3, illustration5, "Get Vaccinated",precautions[2])}
                {this.precautions(4, illustration6, "Avoid Crowds",precautions[3])}
                {this.precautions(5, illustration7, "Wash your hands often",precautions[4])}
                {this.precautions(6, illustration8, "Clean and disinfect",precautions[5])}
                <Footer />
            </div>);
    }
}

export default Information;

