import axios from "axios";

var axiosInstance = axios.create({
  baseURL: 'https://api.covid19api.com',
});

export default axiosInstance;