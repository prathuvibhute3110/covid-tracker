// Importing Dependencies
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import { applyMiddleware , createStore } from 'redux';

// Component
import App from './components/App';
import reducers from './Reducers/index';

// Style
import './Style/index.css';

ReactDOM.render(
    <Provider store={createStore(reducers,applyMiddleware(ReduxThunk))}>
        <App />
    </Provider>,
    document.getElementById('root')
)

