import {combineReducers} from 'redux';

const covidGlobalUpdate = (state=null,action) => {
 
    switch(action.type) {

        case "GET_COVID_DATA":
                return action.payload;

        case "GET_HOSPITAL_DATA":
            return action.payload;
            
        default:
            return state;
    }
}

const covidHospitalData = (state=null,action) => {
 
    switch(action.type) {

        case "GET_HOSPITAL_DATA":
            return action.payload;
            
        default:
            return state;
    }
}

const covidDonorData = (state=null,action) => {

    switch(action.type) {

        case "GET_DONOR_DATA":
            return action.payload;
            
        default:
            return state;
    }
}

export default combineReducers({
    covidData: covidGlobalUpdate,
    hospitalData: covidHospitalData,
    donorData: covidDonorData
});