// Navbar Routes

export const HOME = '/';
export const INFORMATION = '/information';
export const ABOUTUS = '/aboutus';
export const LOGIN = '/login';
export const ANALYTICS = '/analytics';

// Home Page Routes

export const COVIDUPDATE = "/liveupdate";
export const DONORNEARME = "/donornearme";
export const COVIDCARECENTER = "/covidcarecenter";
export const COVIDVACCINEREG = "/covidvaccineregistration";