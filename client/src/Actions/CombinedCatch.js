

const HandleExceptionWithSecureCatch = (exception) => {
    return (dispatch, getState) => {
        var statusCode = exception.response ? exception.response.status : 500;

        switch(statusCode) {

            case 200:
                //Dispatch Action or do some operation to be carried out!
                break;

            case 500:
                //Dispatch Action or do some operation to be carried out!
                break;

            default:
                //Dispatch Action or do some operation to be carried out!
                break;
        }
    }
}

export {
    HandleExceptionWithSecureCatch
};