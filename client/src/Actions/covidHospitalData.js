//import firebase from 'firebase';
import { collection, getDocs } from 'firebase/firestore/lite';
import db from '../components/Firebase/index';

// Secured Combined Catches
import { HandleExceptionWithSecureCatch } from "./CombinedCatch";

const getCovidHospitalData = () => {

    const response=collection(db,'hospital');
    return async (dispatch) => {

//        try {

            const data = await getDocs(response);
            const hospitalList = data.docs.map(doc => doc.data());

            dispatch({
                type: "GET_HOSPITAL_DATA",
                payload: hospitalList
            });
  /*      } catch(e) {
            dispatch(HandleExceptionWithSecureCatch(e));
            dispatch({
                type: "GET_COVID_DATA",
                payload: e.response.data
            })  
        }*/
    }
}

export default getCovidHospitalData;