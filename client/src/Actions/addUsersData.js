// Firebase backend
//import firebase from 'firebase';
// import { collection, addDoc } from 'firebase/firestore/lite';
// import db from '../components/Firebase/index';


// Node backend
import { URL } from '../_Utils/Api';
import axios from "axios";

const addUserData = (promise) => {

    return async () => {
        if (promise.userData.type === 'Patient') {
            await axios.post(`${URL}patient`, promise.userData).then((data) => {
                console.log("Patient: ", data.data);
            }).catch((error) => {
                console.log("Error: ", error)
            });
        } else {
            await axios.post(`${URL}donar`, promise.userData).then((data) => {
                console.log("Donar: ", data.data);
            }).catch((error) => {
                console.log("Error: ", error)
            });
        }

        // Firebase 
        // const data = await addDoc(promise.ref, promise.userData);
    }
}

export default addUserData;