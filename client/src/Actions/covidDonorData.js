//import firebase from 'firebase';
import { collection, getDocs } from 'firebase/firestore/lite';
import db from '../components/Firebase/index';
import axios from 'axios';
import { URL } from '../_Utils/Api';

// Secured Combined Catches

const getCovidDonorData = () => {

    const response = collection(db, 'user');
    return async (dispatch) => {

        const data = await axios.get(`${URL}donar`).then(() => {

        })

        const donorList = data.docs.map(doc => doc.data());

        dispatch({
            type: "GET_DONOR_DATA",
            payload: donorList
        });
    }
}

export default getCovidDonorData;