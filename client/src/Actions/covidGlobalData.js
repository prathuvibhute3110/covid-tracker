import axios from "axios";

// Secured Combined Catches
import { HandleExceptionWithSecureCatch } from "./CombinedCatch";

const getCovidGlobalData = () => {

    return async (dispatch) => {

        try {
            const response = await axios.get("https://api.covid19api.com/summary");
            dispatch({
                type: "GET_COVID_DATA",
                payload: response.data
            })
        } catch(e) {
            dispatch(HandleExceptionWithSecureCatch(e));
            dispatch({
                type: "GET_COVID_DATA",
                payload: e.response.data
            }) 
        }
    }
}

export default getCovidGlobalData;